package com.example.hdmiindemo3;

import com.example.hdmiindemo3.view.HDMIRxPlayer;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AppBaseActivity extends Activity{
	public ViewGroup m_Root;
	public HDMIRxPlayer m_HDMIRxPlayer = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	public void start(View view) {
		m_Root=(ViewGroup) view;
		m_HDMIRxPlayer = new HDMIRxPlayer(this, m_Root, 1920, 1080);
	}
	public void stop() {
		try {
			m_HDMIRxPlayer.stop();
			m_HDMIRxPlayer.release();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
