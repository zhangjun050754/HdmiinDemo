package com.example.hdmiindemo3;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

public class MainActivity extends AppBaseActivity {
	private ViewGroup m_root;
	private Button bt, start_bt, stop_bt;
	private RecordService recordService;
	private static final int RECORD_REQUEST_CODE = 101;
	private static final int STORAGE_REQUEST_CODE = 102;
	private static final int AUDIO_REQUEST_CODE = 103;

	private MediaProjectionManager projectionManager;
	private MediaProjection mediaProjection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		projectionManager = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);
		setContentView(R.layout.activity_main);
		m_root = findViewById(R.id.main_hdmi_view);
		bt = findViewById(R.id.main_bt);
		start_bt = findViewById(R.id.main_start);
		stop_bt = findViewById(R.id.main_stop);
		Intent intent = new Intent(this, RecordService.class);
		bindService(intent, connection, BIND_AUTO_CREATE);

		//start(m_root);
		bt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(MainActivity.this, NextActivity.class));

			}
		});
		start_bt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent captureIntent = projectionManager.createScreenCaptureIntent();
				startActivityForResult(captureIntent, RECORD_REQUEST_CODE);

			}
		});
		stop_bt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				recordService.stopRecord();

			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		start(m_root);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == RECORD_REQUEST_CODE && resultCode == RESULT_OK) {
			mediaProjection = projectionManager.getMediaProjection(resultCode, data);
			recordService.setMediaProject(mediaProjection);
			recordService.startRecord();
		}
	}

	private ServiceConnection connection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);
			RecordService.RecordBinder binder = (RecordService.RecordBinder) service;
			recordService = binder.getRecordService();
			recordService.setConfig(metrics.widthPixels, metrics.heightPixels, metrics.densityDpi);
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
		}
	};

	@Override
	protected void onPause() {
		super.onPause();
		recordService.stopRecord();
		Log.d("3333", "onPause");
		stop();
	}

}
