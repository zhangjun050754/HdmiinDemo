package com.example.hdmiindemo3.view;


import com.example.hdmiindemo3.R;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class HdmiView extends View{
	private static final String TAG	="HdmiView";
	private ViewGroup m_Root;
	private View rootview;
	private HDMIRxPlayer m_HDMIRxPlayer = null;
	private final Handler mHandler = new Handler();
	private Context mContext;

	public HdmiView(Context context) {
		super(context);
		mContext=context;
		LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rootview=inflater.inflate( R.layout.hdmi_view, null);
		
		if (hasRtkMusicPlaybackService()) {
			Intent i = new Intent("com.android.music.musicservicecommand");
			i.putExtra("command", "stop");
			mContext.sendBroadcast(i);
		}
		
		m_Root=rootview.findViewById(R.id.hdmi_view_frame);
		m_HDMIRxPlayer = new HDMIRxPlayer(mContext, m_Root, 1920, 1080);
		
	}
	
	public boolean hasRtkMusicPlaybackService() {
		ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.android.music.MediaPlaybackService".equals(service.service.getClassName()) && service.foreground) {
				Log.i("BGMusic", "\033[0;31;31m MusicPlayback is running \033[m");
				return true;
			}
		}
		Log.i("BGMusic", "\033[0;31;31m No MusicPlayback is running \033[m");
		return false;
	}
	
}
